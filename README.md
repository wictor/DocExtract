DocExtract tool
===============

Simple console tool extracting plain text data from office documents.

Primarily used to track changes in svn-versioned documents. Just data matters, ignoring any format or formula.

Supported formats
-----------------

* ODS
* XSLX

License
-------
Licensed under the [GPL](http://www.gnu.org/licenses/gpl.html).