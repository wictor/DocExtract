package cz.sysrq.docextract;

import cz.sysrq.docextract.data.ExcelExtractSpec;

import java.io.IOException;

/**
 * DocumentExtractor common interface.
 * <p/>
 * Created by vsvoboda on 16.1.2016.
 */
public interface DocumentExtractor {

    /**
     * Reads specified file and extracts plain text values.
     *
     * @param spec file and extraction specification
     * @throws IOException
     */
    void read(ExcelExtractSpec spec) throws IOException;
}
