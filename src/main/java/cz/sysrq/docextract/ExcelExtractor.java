package cz.sysrq.docextract;

import cz.sysrq.docextract.data.CellSpec;
import cz.sysrq.docextract.data.ExcelExtractSpec;
import cz.sysrq.docextract.data.SheetExtractSpec;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

/**
 * MS Open XML Excel file format handler.
 *
 * Created by vsvoboda on 16.1.2016.
 */
public class ExcelExtractor extends StreamOutputtingExtractorBase {

    private final Logger logger = LoggerFactory.getLogger(ExcelExtractor.class);

    /**
     * Excel stores results of formula type cells. It should be used not only to improve performance,
     * but also to avoid errors in complicated formulas, where POI lib formula evaluation crashes.
     */
    private final boolean useCachedResults;

    public ExcelExtractor(OutputStream outputStream, boolean useCachedResults) {
        super(outputStream);
        this.useCachedResults = useCachedResults;
    }

    public ExcelExtractor(OutputStream outputStream) {
        // use cached results by defualt
        this(outputStream, true);
    }

    public void read(ExcelExtractSpec spec) throws IOException {
        logger.debug("Reading {}", spec);
        out(spec.getFile().getName() + ";");
        Workbook wb = new XSSFWorkbook(new FileInputStream(spec.getFile()));
        FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();
        for (SheetExtractSpec sheetSpec : spec.getSheets()) {
            Sheet sheet = wb.getSheet(sheetSpec.getSheet());
            if (sheet == null) {
                throw new NoSuchElementException("Sheet not found: " + sheetSpec.getSheet());
            }
            List<CellSpec> cellSpecList = sheetSpec.getCellSpecList();
            for (CellSpec cellSpec : cellSpecList) {
                Row row = sheet.getRow(cellSpec.getRow() - 1);
                Cell cell = row.getCell(cellSpec.getCol() - 1);
                out(getCellValue(cell, evaluator, useCachedResults) + ";");
                // TODO VSV: value delimiter should be parametrized (';' vs ',')
            }
        }
        nl();
    }

    private static String getCellNumValue(Cell cell) {
        if (cell != null) {
            double doubleVal = cell.getNumericCellValue();
            BigDecimal bd = BigDecimal.valueOf(doubleVal);
            BigDecimal rounded = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
            // TODO VSV: locale shouldn't be hardcoded
            NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
            nf.setGroupingUsed(false);
            return nf.format(rounded);
        }
        return "null";
    }

    private static String getCellValue(Cell cell, FormulaEvaluator evaluator, boolean useCachedResults) {
        if (cell != null) {
            int cellType = cell.getCellType();
            // if cell contains a formula, compute its result
            if (cellType == Cell.CELL_TYPE_FORMULA) {
                // formula - use cached result
                if (useCachedResults) {
                    int resultType = cell.getCachedFormulaResultType();
                    return getCellStringValue(resultType, cell);
                }
                // formula - evaluate result now
                int evaluatedCellType = evaluator.evaluateFormulaCell(cell);
                return getCellStringValue(evaluatedCellType, cell);
            } else {
                return getCellStringValue(cellType, cell);
            }

        } else {
            return "null";
        }
    }

    private static String getCellStringValue(int cellType, Cell cell) {
        switch (cellType) {
            case Cell.CELL_TYPE_BOOLEAN:
                return Boolean.toString(cell.getBooleanCellValue());
            case Cell.CELL_TYPE_NUMERIC:
                return getCellNumValue(cell);
            case Cell.CELL_TYPE_STRING:
                return cell.getStringCellValue();
            case Cell.CELL_TYPE_BLANK:
                return "";
            case Cell.CELL_TYPE_ERROR:
                return String.valueOf(cell.getErrorCellValue());
            case Cell.CELL_TYPE_FORMULA:
                // we are detecting result type of non-formula cell or result type of evaluated formula cell here
                throw new IllegalStateException("Formula type shouldn't occur here!");
            default:
                return cell.toString();
        }
    }

}
