package cz.sysrq.docextract.data;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Excel sheet extract specification.
 * <p/>
 * Created by vsvoboda on 11.1.2016.
 */
public class SheetExtractSpec {

    private final String sheet;
    private final List<CellSpec> cellSpecList;

    public SheetExtractSpec(String sheet, List<CellSpec> cellSpecList) {
        this.sheet = sheet;
        this.cellSpecList = cellSpecList;
    }

    public String getSheet() {
        return sheet;
    }

    public List<CellSpec> getCellSpecList() {
        return cellSpecList;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (CellSpec cs : cellSpecList) {
            if (sb.length() > 1) {
                sb.append(",");
            }
            sb.append(cs.toString());
        }
        return String.format("%s!%s", sheet, sb.toString());
    }

    public static SheetExtractSpec valueOf(String sheetCoord) {
        Pattern ptn = Pattern.compile("(?<name>[A-Za-z0-9_]+)!(?<cells>[A-Z0-9,]+)");
        Matcher matcher = ptn.matcher(sheetCoord);
        if (matcher.matches()) {
            String sheetName = matcher.group("name");
            String cells = matcher.group("cells");
            String[] cellsArr = cells.split(",");
            List<CellSpec> list = new ArrayList<>(cellsArr.length);
            for (String cell : cellsArr) {
                list.add(CellSpec.valueOf(cell));
            }
            return new SheetExtractSpec(sheetName, list);
        }
        throw new IllegalArgumentException("Illegal sheet specification " + sheetCoord);
    }
}
