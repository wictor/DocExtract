package cz.sysrq.docextract.data;

import java.io.File;
import java.util.Objects;

/**
 * Ancestor
 *
 * Created by vsvoboda on 11.1.2016.
 */
public class ExtractSpec {

    protected final File file;

    public ExtractSpec(File file) {
        this.file = file;
    }

    public File getFile() {
        return file;
    }

    /**
     * @return {@code true} if all values from the document should be dumped
     */
    public boolean readAll() {
        return true;
    }

    @Override
    public String toString() {
        return Objects.toString(file);
    }
}
