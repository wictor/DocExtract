package cz.sysrq.docextract.data;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Excel cell specification.
 *
 * Created by vsvoboda on 11.1.2016.
 */
public class CellSpec {

    private final int col;
    private final int row;

    public CellSpec(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    @Override
    public String toString() {
        int colInt = col - 1 + (int) 'A';
        char colChar = (char) colInt;
        return String.format("%c%d", colChar, row);
    }

    public static CellSpec valueOf(String coord) {
        Pattern ptn = Pattern.compile("(?<col>[A-Z]+)(?<row>[0-9]+)");
        Matcher matcher = ptn.matcher(coord);
        if (matcher.matches()) {
            String cs = matcher.group("col");
            int c = parseCol(cs);
            int r = Integer.parseInt(matcher.group("row"));

            return new CellSpec(c, r);
        }
        throw new IllegalArgumentException("Illegal cell specification " + coord);
    }

    private static int parseCol(String columnString) {
        // TODO VSV: implement parsing of multiple chars ...like "AA"
        if (columnString.length() != 1) {
            throw new IllegalArgumentException("Cannot compute column index for specification " + columnString);
        }

        char ch = columnString.charAt(0);
        return (int)ch - (int)'A' + 1;
    }
}
