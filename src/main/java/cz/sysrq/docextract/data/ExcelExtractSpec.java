package cz.sysrq.docextract.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Excel and ODS extract specification.
 *
 * Created by vsvoboda on 11.1.2016.
 */
public class ExcelExtractSpec extends ExtractSpec {

    private final List<SheetExtractSpec> sheets;

    public ExcelExtractSpec(File file, List<SheetExtractSpec> sheetList) {
        super(file);
        this.sheets = sheetList;
    }

    public ExcelExtractSpec(File file, SheetExtractSpec... sheets) {
        this(file, Arrays.asList(sheets));
    }

    public List<SheetExtractSpec> getSheets() {
        return sheets;
    }

    @Override
    public boolean readAll() {
        return sheets.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (SheetExtractSpec cs : sheets) {
            if (sb.length() > 1) {
                sb.append(";");
            }
            sb.append(cs.toString());
        }
        return String.format("%s %s", file.toString(), sb.toString());
    }

    public static ExcelExtractSpec valueOf(File file, String sheetsSpec) {
        String[] sheetsSpecArr = sheetsSpec.split(";");
        List<SheetExtractSpec> list = new ArrayList<>(sheetsSpecArr.length);
        for (String sh : sheetsSpecArr) {
            list.add(SheetExtractSpec.valueOf(sh));
        }
        return new ExcelExtractSpec(file, list);
    }
}
