package cz.sysrq.docextract.data;

import java.io.File;
import java.nio.file.Path;

/**
 * Document type enumeration.
 *
 * Created by vsvoboda on 16.1.2016.
 */
public enum DocType {

    OPEN_DOCUMENT_SPREADSHEET("ODS"),
    OPEN_DOCUMENT_TEXT("ODT"),
    MS_OPEN_XML_EXCEL("XLSX"),
    MS_OPEN_XML_EXCEL_MACROS("XLSM"),
    MS_EXCEL("XLS");

    private final String ext;

    DocType(String ext) {
        this.ext = ext;
    }

    public static DocType byExtension(String ext) {
        String extUpper = ext.toUpperCase();
        for (DocType type : values()) {
            if (type.ext.equals(extUpper)) {
                return type;
            }
        }
        throw new IllegalArgumentException("Unknown extension " + ext);
    }

    public static DocType byFile(File file) {
        String fileName = file.getName();
        String extension = "";
        int extensionPos = fileName.lastIndexOf('.');
        if (extensionPos > 1) {
            extension = fileName.substring(extensionPos + 1);
        }
        return byExtension(extension);
    }
}
