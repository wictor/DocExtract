package cz.sysrq.docextract;

import cz.sysrq.docextract.data.CellSpec;
import cz.sysrq.docextract.data.ExcelExtractSpec;
import cz.sysrq.docextract.data.SheetExtractSpec;
import org.jopendocument.dom.spreadsheet.Cell;
import org.jopendocument.dom.spreadsheet.Sheet;
import org.jopendocument.dom.spreadsheet.SpreadSheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.List;

/**
 * ODF-ODS format handler.
 */
public class OdsExtractor extends StreamOutputtingExtractorBase {

    private final Logger logger = LoggerFactory.getLogger(OdsExtractor.class);

    public OdsExtractor(OutputStream outputStream) {
        super(outputStream);
    }

    public void read(ExcelExtractSpec spec) throws IOException {
        logger.debug("Reading {}", spec);
        if (spec.readAll()) {
            readAll(spec.getFile());
        } else {
            readExtract(spec);
        }
    }

    private void readExtract(ExcelExtractSpec spec) throws IOException {
        out(spec.getFile().getName() + ":");
        SpreadSheet spreadSheet = SpreadSheet.createFromFile(spec.getFile());
        for (SheetExtractSpec sheetSpec : spec.getSheets()) {
            Sheet sheet = spreadSheet.getSheet(sheetSpec.getSheet(), true);
            List<CellSpec> cellSpecList = sheetSpec.getCellSpecList();
            for (CellSpec cellSpec : cellSpecList) {
                Cell<SpreadSheet> cell = sheet.getImmutableCellAt(cellSpec.getCol() - 1, cellSpec.getRow() - 1);
                String textValue = cell.getTextValue();
                out(textValue + ",");
            }
        }
        nl();
    }

    public void readAll(File file) throws IOException {
        SpreadSheet spreadSheet = SpreadSheet.createFromFile(file);
        int sheetCount = spreadSheet.getSheetCount();
        for (int i = 0; i < sheetCount; i++) {
            Sheet sheet = spreadSheet.getSheet(i);
            out("Sheet \"" + sheet.getName() + "\":");
            nl();
            readAll(sheet);
        }
    }

    private void readAll(Sheet sheet) throws IOException {
        int rowCount = sheet.getRowCount();
        int columnCount = sheet.getColumnCount();

        int lastNonEmptyRow = -1;
        boolean skippingEmptyRows = false;
        for (int r = 0; r < rowCount; r++) {
            int lastNonEmptyColumn = -1;
            boolean skippingEmptyColumns = false;
            boolean emptyRow = true;
            for (int c = 0; c < columnCount; c++) {
                Cell<SpreadSheet> cell = sheet.getImmutableCellAt(c, r);
                String textValue = cell.getTextValue();
                if (textValue.isEmpty()) {
                    skippingEmptyColumns = true;
                } else {
                    // row is not empty and now is about to print the first data
                    // -> print skipped row info
                    if (skippingEmptyRows) {
                        out(String.format("%d empty rows", r - 1 - lastNonEmptyRow));
                        nl();
                    }
                    // -> print row header
                    if (lastNonEmptyColumn < 0) {
                        out(String.format("%04d: ", r));
                    }
                    if (skippingEmptyColumns) {
                        out(String.format("%d empty columns,", c - 1 - lastNonEmptyColumn));
                    }
                    lastNonEmptyRow = r;
                    lastNonEmptyColumn = c;
                    skippingEmptyRows = false;
                    skippingEmptyColumns = false;
                    emptyRow = false;
                    out(textValue + ",");
                }
            }
            if (emptyRow) {
                skippingEmptyRows = true;
            } else {
                if (skippingEmptyColumns) {
                    out(String.format("%d empty columns,", columnCount - 1 - lastNonEmptyColumn));
                }
                nl();
            }
        }
        if (skippingEmptyRows) {
            out(String.format("%d empty rows", rowCount - 1 - lastNonEmptyRow));
            nl();
        }
    }

}
