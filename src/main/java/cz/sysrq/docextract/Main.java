package cz.sysrq.docextract;

import cz.sysrq.docextract.data.DocType;
import cz.sysrq.docextract.data.ExcelExtractSpec;
import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Application entry point.
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        Options options = createApplicationOptions();
        CommandLine commandLine;
        CommandLineParser parser = new DefaultParser();
        try {
            commandLine = parser.parse(options, args);
            if (commandLine != null) {
                process(commandLine);
            }
        } catch (ParseException e) {
            System.err.println("Argument error: " + e.getMessage());
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("docextract", "header", options, "footer", true);
        }
    }

    private static void process(CommandLine commandLine) throws IOException {
        String inputFileNameMask = commandLine.getOptionValue("i");
        String extractSpecString = commandLine.getOptionValue("e");
        String outputFileString = commandLine.getOptionValue("o");

        logger.debug("inputFileNameMask: {}", inputFileNameMask);
        logger.debug("extractSpecString: {}", extractSpecString);
        logger.debug("outputFileString: {}", outputFileString);

        // TODO VSV: break the current directory restriction
        List<File> files = matchingFiles(Paths.get("."), inputFileNameMask);
        // all files should be the same type
        DocType docType = null;
        for (File file : files) {
            DocType fileType = DocType.byFile(file);
            logger.debug("Checking file {} -> detected type {}", file.getName(), fileType);
            if (docType != null && !docType.equals(fileType)) {
                throw new IllegalArgumentException("File type mismatched : " + file.getName() + " is not of type " + docType);
            }
            docType = fileType;
        }
        if (files.isEmpty()) {
            throw new IllegalArgumentException("No files matched for parameter " + inputFileNameMask);
        }
        logger.info("Detected document type: {}", docType);
        Objects.requireNonNull(docType);

        try (OutputStream outputStream = new FileOutputStream(outputFileString)) {
            // TODO VSV: use some kind of abstraction
            DocumentExtractor extractor;
            switch (docType) {
                case OPEN_DOCUMENT_SPREADSHEET:
                    extractor = new OdsExtractor(outputStream);
                    for (File file : files) {
                        extractor.read(ExcelExtractSpec.valueOf(file, extractSpecString));
                    }
                    break;
                case MS_OPEN_XML_EXCEL:
                case MS_OPEN_XML_EXCEL_MACROS:
                    extractor = new ExcelExtractor(outputStream);
                    for (File file : files) {
                        extractor.read(ExcelExtractSpec.valueOf(file, extractSpecString));
                    }
                    break;
                default:
                    throw new IllegalStateException("File type not supported yet.");
            }
        }
    }


    static List<File> matchingFiles(Path path, String filePattern) throws IOException {
        PathMatcher matcher = FileSystems.getDefault().getPathMatcher("glob:" + filePattern);
        List<File> files = new LinkedList<>();
        try (DirectoryStream<Path> directoryStream = Files.newDirectoryStream(path)) {
            for (Path filePath : directoryStream) {
                if (matcher.matches(filePath)) {
                    File file = filePath.toFile();
                    if (file.isFile()) {
                        files.add(file);
                    }
                }
            }
        }
        return files;
    }

    private static Options createApplicationOptions() {
        Options options = new Options();
        options.addOption(Option.builder("i").longOpt("input").hasArg().desc("specify input document file (mask)").required().build());
        options.addOption(Option.builder("e").longOpt("extract").hasArg().desc("parts to extract").build());
        options.addOption(Option.builder("o").longOpt("output").hasArg().desc("output file").required().build());
        return options;
    }

}
