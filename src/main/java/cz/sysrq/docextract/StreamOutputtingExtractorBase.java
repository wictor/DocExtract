package cz.sysrq.docextract;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

/**
 * Common extractor ancestor.
 * <p/>
 * Created by vsvoboda on 16.1.2016.
 */
public abstract class StreamOutputtingExtractorBase implements DocumentExtractor {

    private final BufferedWriter writer;

    public StreamOutputtingExtractorBase(OutputStream outputStream) {
        writer = new BufferedWriter(new OutputStreamWriter(outputStream));
    }

    /**
     * Prints a string to output (without newline character).
     *
     * @param output output string
     * @throws IOException
     */
    protected final void out(String output) throws IOException {
        writer.write(output);
    }

    /**
     * Puts newline to output and flushes buffer.
     *
     * @throws IOException
     */
    protected final void nl() throws IOException {
        writer.newLine();
        writer.flush();
    }

}
