package cz.sysrq.docextract.data;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;

/**
 * Unit test
 * <p/>
 * Created by vsvoboda on 13.1.2016.
 */
public class SheetExtractSpecTest {

    @Test
    public void testValueOf_simple() {
        SheetExtractSpec spec = SheetExtractSpec.valueOf("Sheet1!B3");
        assertEquals("Sheet1", spec.getSheet());
        assertEquals(1, spec.getCellSpecList().size());
        assertEquals(2, spec.getCellSpecList().get(0).getCol());
        assertEquals(3, spec.getCellSpecList().get(0).getRow());
    }

    @Test
    public void testValueOf_complex() {
        SheetExtractSpec spec = SheetExtractSpec.valueOf("Sheet9!B3,D7,D9");
        assertEquals("Sheet9", spec.getSheet());
        assertEquals(3, spec.getCellSpecList().size());

        assertEquals(2, spec.getCellSpecList().get(0).getCol());
        assertEquals(3, spec.getCellSpecList().get(0).getRow());

        assertEquals(4, spec.getCellSpecList().get(1).getCol());
        assertEquals(7, spec.getCellSpecList().get(1).getRow());

        assertEquals(4, spec.getCellSpecList().get(2).getCol());
        assertEquals(9, spec.getCellSpecList().get(2).getRow());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValueOf_exc() {
        SheetExtractSpec.valueOf("Sheet9.B3");
    }

    @Test
    public void testToString() throws Exception {
        assertEquals("Sheet1!A1,A2", new SheetExtractSpec("Sheet1", Arrays.asList(new CellSpec(1, 1), new CellSpec(1, 2))).toString());
    }
}