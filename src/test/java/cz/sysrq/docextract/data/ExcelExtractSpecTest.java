package cz.sysrq.docextract.data;

import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Unit test
 * <p/>
 * Created by vsvoboda on 13.1.2016.
 */
public class ExcelExtractSpecTest {

    @Test
    public void testValueOf_simple() throws Exception {
        ExcelExtractSpec spec = ExcelExtractSpec.valueOf(new File("abc.xlsx"), "sh1!A1");
        assertEquals("abc.xlsx", spec.getFile().getName());
        assertEquals(1, spec.getSheets().size());
        assertEquals("sh1", spec.getSheets().get(0).getSheet());
    }

    @Test
    public void testValueOf_complex() throws Exception {
        ExcelExtractSpec spec = ExcelExtractSpec.valueOf(new File("abc.xlsx"), "sh1!A1;sh2!B3,E7");
        assertEquals("abc.xlsx", spec.getFile().getName());
        assertEquals(2, spec.getSheets().size());
        assertEquals("sh1", spec.getSheets().get(0).getSheet());
        assertEquals("sh2", spec.getSheets().get(1).getSheet());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValueOf_exc() throws Exception {
        ExcelExtractSpec.valueOf(new File("abc.xlsx"), "sh1!A1 sh2!B3,E7");
    }

    @Test
    public void testToString() throws Exception {
        List<SheetExtractSpec> list = new ArrayList<>();
        list.add(new SheetExtractSpec("Sheet1", Arrays.asList(new CellSpec(1, 1), new CellSpec(1, 2))));
        list.add(new SheetExtractSpec("Sheet5", Arrays.asList(new CellSpec(2, 1), new CellSpec(2, 2))));
        ExcelExtractSpec excelExtractSpec = new ExcelExtractSpec(new File("abc.xlsx"), list);
        assertEquals("abc.xlsx Sheet1!A1,A2;Sheet5!B1,B2", excelExtractSpec.toString());
    }

}