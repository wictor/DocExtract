package cz.sysrq.docextract.data;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test
 * <p/>
 * Created by vsvoboda on 12.1.2016.
 */
public class CellSpecTest {

    @Test
    public void testValueOf_simple() throws Exception {
        CellSpec a1 = CellSpec.valueOf("A1");
        assertEquals(1, a1.getCol());
        assertEquals(1, a1.getRow());

        CellSpec z9 = CellSpec.valueOf("Z9");
        assertEquals(26, z9.getCol());
        assertEquals(9, z9.getRow());
    }

    @Test @Ignore // not implemented yet
    public void testValueOf_complex() throws Exception {
        CellSpec a1 = CellSpec.valueOf("AA150");
        assertEquals(27, a1.getCol());
        assertEquals(150, a1.getRow());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testValueOf_exc() throws Exception {
        CellSpec.valueOf("a1");
    }

    @Test
    public void testToString() {
        assertEquals("A1", new CellSpec(1, 1).toString());
        assertEquals("C9", new CellSpec(3, 9).toString());
    }
}