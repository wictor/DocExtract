package cz.sysrq.docextract;


import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Unit test for {@link Main} class.
 */
public class MainTest {

    @Test @Ignore // now only current directory files supported
    public void testMain() throws Exception {
        String[] args = {"-i", "src/test/resources/test1sheets.ods"};
        Main.main(args);
    }

    @Test
    public void testArgs() throws Exception {
        String[] args = {"-x"};
        Main.main(args);
    }

    @Test
    public void testMatchingFiles() throws IOException {
        Path path = Paths.get("src/test/resources");
        String pat = "src/test/resources/t*.ods";

        List<File> files = Main.matchingFiles(path, pat);

        Assert.assertEquals(2, files.size());
    }

}
