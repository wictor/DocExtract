package cz.sysrq.docextract;

import cz.sysrq.docextract.data.ExcelExtractSpec;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;

/**
 * {@link ExcelExtractor} unit test.
 *
 * Created by vsvoboda on 16.1.2016.
 */
public class ExcelExtractorTest {

    private ByteArrayOutputStream outputStream;
    private ExcelExtractor extractor;

    @Before
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        extractor = new ExcelExtractor(outputStream);
    }

    @Test
    public void testRead() throws Exception {
        File file = new File("src/test/resources/test5excel.xlsx");
        extractor.read(ExcelExtractSpec.valueOf(file, "DEV!A3,B3"));
        String out = outputStream.toString();
        String[] outputLines = out.split(System.lineSeparator());

        String[] expected = {
                "test5excel.xlsx;Michal;MSK;"
        };

        Assert.assertArrayEquals(expected, outputLines);
    }

    @Test
    public void testRead_Numeric() throws Exception {
        File file = new File("src/test/resources/test5excel.xlsx");
        extractor.read(ExcelExtractSpec.valueOf(file, "DEV!A3;List3!A3,A6,B3,B7"));
        String out = outputStream.toString();
        String[] outputLines = out.split(System.lineSeparator());

        String[] expected = {
                "test5excel.xlsx;Michal;1235;123,5;617,5;32,66;"
        };

        Assert.assertArrayEquals(expected, outputLines);
    }
}