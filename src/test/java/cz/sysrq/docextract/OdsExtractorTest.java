package cz.sysrq.docextract;

import cz.sysrq.docextract.data.ExcelExtractSpec;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.NoSuchElementException;

/**
 * {@link OdsExtractor} unit test.
 */
public class OdsExtractorTest {

    private ByteArrayOutputStream outputStream;
    private OdsExtractor extractor;

    @Before
    public void setup() {
        outputStream = new ByteArrayOutputStream();
        extractor = new OdsExtractor(outputStream);
    }

    @Test
    public void testRead() throws Exception {
        File file = new File("src/test/resources/test1sheets.ods");
        extractor.readAll(file);
        String out = outputStream.toString();
        String[] outputLines = out.split(System.lineSeparator());

        String[] expected = {
                "Sheet \"List1\":",
                "0000: 2015,11 empty columns,",
                "0001: LEDEN,ÚNOR,BŘEZEN,DUBEN,KVĚTEN,ČERVEN,ČERVENEC,SRPEN,ZÁŘÍ,ŘÍJEN,LISTOPAD,PROSINEC,",
                "1 empty rows",
                "0003: postřik proti kadeřavosti broskvoní,3 empty columns,zpracování kompostu,7 empty columns,",
                "0004: 1 empty columns,postřik meruněk,10 empty columns,",
                "26 empty rows",
                "Sheet \"List2\":",
                "0000: List2,2 empty columns,",
                "0001: Zkouška UTF znaků,2 empty columns,",
                "2 empty rows",
                "0004: 2 empty columns,C5,",
                "Sheet \"List3\":",
                "1 empty rows"
        };

        Assert.assertArrayEquals(expected, outputLines);
    }

    @Test
    public void testOdsProblem() throws Exception {
        File file = new File("src/test/resources/test2exp.ods");
        extractor.readAll(file);
        String out = outputStream.toString();
        String[] outputLines = out.split(System.lineSeparator());

        String[] expected = {
                "Sheet \"2009\":",
                "0000: Výdajový list – benzín,4 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,",
                "0003: 1,20.12.2009,17:03,Bruto,1 186,40 Kč,",
                "0004: 2,31.12.2009,11:54,Shell,1 175,90 Kč,",
                "1048571 empty rows",
                "Sheet \"2010\":",
                "0000: Výdajový list – potraviny,4 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,",
                "0003: 1,21.01.2010,18:31,Shell,1 073,90 Kč,",
                "0004: 2,25.01.2010,17:56,Shell,1 033,90 Kč,",
                "0005: 3,09.02.2010,18:49,Shell,655,30 Kč,",
                "0006: 4,14.02.2010,20:37,Shell,811,70 Kč,",
                "0007: 5,05.03.2010,19:08,Shell,916,50 Kč,",
                "0008: 6,14.03.2010,16:45,Shell,1 333,00 Kč,",
                "0009: 7,04.04.2010,09:02,Shell,1 083,30 Kč,",
                "0010: 8,18.04.2010,13:05,Shell,1 401,60 Kč,",
                "0011: 9,20.04.2010,00:00,Pneu,280,00 Kč,",
                "0012: 10,01.05.2010,10:48,Benzina,1 463,00 Kč,",
                "0013: 11,13.05.2010,21:09,Shell,999,70 Kč,",
                "0014: 12,21.05.2010,19:14,Shell,1 218,90 Kč,",
                "0015: 13,29.05.2010,09:42,Shell,1 416,30 Kč,",
                "0016: 14,23.06.2010,19:50,Shell,1 288,20 Kč,",
                "0017: 15,02.07.2010,20:11,Shell,1 209,40 Kč,",
                "0018: 16,09.07.2010,06:58,Shell,1 502,00 Kč,",
                "0019: 17,20.07.2010,18:09,Shell,1 443,50 Kč,",
                "0020: 18,26.07.2010,13:57,Eni oil,1 428,20 Kč,",
                "0021: 19,06.08.2010,18:03,Agip,1 372,60 Kč,",
                "0022: 20,14.08.2010,07:23,OMV,860,70 Kč,",
                "0023: 21,15.08.2010,18:15,Shell,1 092,10 Kč,",
                "0024: 22,22.08.2010,13:20,Shell,1 392,00 Kč,",
                "0025: 23,05.09.2010,15:25,Shell,1 283,70 Kč,",
                "0026: 24,16.10.2010,13:18,Shell,1 358,10 Kč,",
                "0027: 25,23.10.2010,13:29,1 empty columns,827,00 Kč,",
                "0028: 26,13.11.2010,10:00,Shell,1 307,00 Kč,",
                "0029: 27,26.11.2010,07:35,Shell,1 404,90 Kč,",
                "0030: 28,22.12.2010,12:29,Shell,1 333,00 Kč,",
                "0031: 4 empty columns,32 789,50 Kč,",
                "65504 empty rows",
                "Sheet \"2011\":",
                "0000: Výdajový list – potraviny,4 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,",
                "0003: 1,03.02.2011,07:44,OMV,2 624,80 Kč,",
                "0004: 2,11.02.2011,13:49,Agip,551,10 Kč,",
                "0005: 3,25.02.2011,07:45,Agip,1 402,00 Kč,",
                "0006: 4,19.03.2011,15:36,Shell,1 327,30 Kč,",
                "0007: 5,01.04.2011,21:52,FAMA,1 581,00 Kč,",
                "0008: 6,18.04.2011,18:26,Shell,1 679,30 Kč,",
                "0009: 7,21.05.2011,07:55,Shell,1 505,60 Kč,",
                "0010: 8,12.06.2011,08:21,OMV,1 510,30 Kč,",
                "0011: 9,30.06.2011,20:09,Shell,1 355,70 Kč,",
                "0012: 10,16.07.2011,15:26,Pasoil,1 468,20 Kč,",
                "0013: 11,26.07.2011,12:35,1 empty columns,1 199,86 Kč,",
                "0014: 12,31.07.2011,19:20,Ahold,1 192,50 Kč,",
                "0015: 13,03.09.2011,16:05,Bruto,1 494,80 Kč,",
                "0016: 14,14.09.2011,19:48,Bruto,1 512,50 Kč,",
                "0017: 15,25.09.2011,09:36,Bruto,1 497,40 Kč,",
                "0018: 16,02.10.2011,14:44,1 empty columns,1 153,40 Kč,",
                "0019: 17,22.10.2011,16:13,PapOil,1 403,80 Kč,",
                "0020: 18,30.10.2011,16:09,Shell,1 087,50 Kč,",
                "0021: 19,20.11.2011,15:25,Shell,1 592,40 Kč,",
                "0022: 20,15.12.2011,16:24,PapOil,1 520,50 Kč,",
                "0023: 21,31.12.2011,09:07,Bruto,1 361,80 Kč,",
                "0024: 4 empty columns,30 021,76 Kč,",
                "65511 empty rows",
                "Sheet \"2012\":",
                "0000: Výdajový list – benzín,4 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,",
                "0003: 1,28.01.2012,13:13,PAP OIL,2 910,00 Kč,",
                "0004: 2,29.02.2012,18:00,Bruto,1 313,50 Kč,",
                "0005: 3,14.03.2012,19:02,Bruto,1 526,10 Kč,",
                "0006: 4,07.04.2012,23:11,Bruto,1 395,60 Kč,",
                "0007: 5,16.04.2012,18:13,Bruto,1 515,00 Kč,",
                "0008: 6,24.06.2012,19:27,OMV,1 775,10 Kč,",
                "0009: 7,10.07.2012,17:02,Bruto,1 658,80 Kč,",
                "0010: 8,27.07.2012,17:41,Bruto,805,60 Kč,",
                "0011: 9,09.08.2012,08:55,Bruto,1 339,40 Kč,",
                "0012: 10,12.08.2012,16:16,Bruto,984,70 Kč,",
                "0013: 11,04.11.2012,17:35,Shell,1 527,20 Kč,",
                "0014: 12,12.12.2012,12:05,Bruto,1 393,90 Kč,",
                "0015: 13,07.06.2012,19:31,Bruto,1 567,80 Kč,",
                "0016: 4 empty columns,19 712,70 Kč,",
                "1048559 empty rows",
                "Sheet \"2013\":",
                "0000: Výdajový list – benzín,4 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,",
                "0003: 1,01.01.2013,16:21,Bruto,1 415,20 Kč,",
                "0004: 2,17.02.2013,20:04,Bruto,1 554,50 Kč,",
                "0005: 3,16.03.2013,17:24,Bruto,1 578,30 Kč,",
                "0006: 4,30.03.2013,17:19,Bruto,574,00 Kč,",
                "0007: 5,14.04.2013,14:34,Shell,1 615,20 Kč,",
                "0008: 6,10.05.2013,15:17,Autorest U,1 373,10 Kč,",
                "0009: 7,24.05.2013,18:21,Bruto,962,50 Kč,",
                "0010: 8,20.06.2013,08:01,Bruto,501,20 Kč,",
                "0011: 9,30.06.2013,08:23,Pasoil,1 503,80 Kč,",
                "0012: 10,10.07.2013,17:43,Bruto,1 625,20 Kč,",
                "0013: 11,21.07.2013,16:28,Bruto,1 107,80 Kč,",
                "0014: 12,26.07.2013,19:52,ETK,1 738,00 Kč,",
                "0015: 13,16.08.2013,17:19,Bruto,1 046,50 Kč,",
                "0016: 14,28.08.2013,20:55,Bruto,1 407,80 Kč,",
                "0017: 15,07.09.2013,20:40,Bruto,1 643,60 Kč,",
                "0018: 16,04.10.2013,18:43,Bruto,795,90 Kč,",
                "0019: 17,28.10.2013,14:29,Shell,574,80 Kč,",
                "0020: 18,03.11.2013,19:57,Bruto,499,00 Kč,",
                "0021: 19,06.11.2013,12:25,Robin Oil,20,00 Kč,",
                "0022: 20,12.11.2013,10:30,ČS JIMO,1 568,00 Kč,",
                "0023: 21,07.12.2013,08:39,Bruto,1 548,20 Kč,",
                "0024: 22,25.12.2013,17:30,Bruto,1 245,10 Kč,",
                "0025: 4 empty columns,25 897,70 Kč,",
                "1048550 empty rows",
                "Sheet \"2014\":",
                "0000: Výdajový list – benzín,5 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,1 empty columns,",
                "0003: 1,12.01.2014,1 empty columns,Bruto,595,30 Kč,1 empty columns,",
                "0004: 2,31.01.2014,18:42,Bruto,1 286,80 Kč,1 empty columns,",
                "0005: 3,19.02.2014,17:23,Bruto,1 452,90 Kč,1 empty columns,",
                "0006: 4,13.03.2014,09:36,Bruto,1 389,40 Kč,1 empty columns,",
                "0007: 5,19.04.2014,16:25,Bruto,1 479,40 Kč,1 empty columns,",
                "0008: 6,07.05.2014,17:19,Bruto,1 438,60 Kč,1 empty columns,",
                "0009: 7,30.05.2014,16:44,Bruto,1 400,20 Kč,Tvoje,",
                "0010: 8,13.06.2014,16:19,Bruto,1 395,80 Kč,1 empty columns,",
                "0011: 9,11.07.2014,19:38,Pap Oil,1 539,90 Kč,1 empty columns,",
                "0012: 10,12.08.2014,18:01,Bruto,1 699,90 Kč,1 empty columns,",
                "0013: 11,28.08.2014,18:11,Bruto,1 578,20 Kč,1 empty columns,",
                "0014: 12,07.09.2014,17:20,Bruto,1 350,30 Kč,1 empty columns,",
                "0015: 13,19.09.2014,16:31,Bruto,919,10 Kč,1 empty columns,",
                "0016: 14,10.10.2014,18:37,Bruto,1 544,10 Kč,1 empty columns,",
                "0017: 15,28.10.2014,15:34,Bruto,1 539,70 Kč,1 empty columns,",
                "0018: 16,16.11.2014,08:40,Bruto,1 559,10 Kč,1 empty columns,",
                "0019: 17,16.12.2014,19:54,Bruto,1 454,80 Kč,1 empty columns,",
                "0020: 4 empty columns,23 623,50 Kč,1 empty columns,",
                "1048555 empty rows",
                "Sheet \"2015\":",
                "0000: Výdajový list – benzín,5 empty columns,",
                "1 empty rows",
                "0002: číslo,datum,čas,prodejna,částka,1 empty columns,",
                "0003: 1,05.01.2015,07:45,Bruto ,839,80 Kč,1 empty columns,",
                "0004: 2,14.02.2015,08:10,Bruto ,2 737,50 Kč,1 empty columns,",
                "0005: 3,01.03.2015,15:29,Bruto ,802,80 Kč,1 empty columns,",
                "0006: 4,14.03.2015,14:13,Bruto ,995,00 Kč,1 empty columns,",
                "0007: 5,11.04.2015,13:50,Bruto ,1 254,80 Kč,1 empty columns,",
                "0008: 6,01.05.2015,10:56,Bruto ,785,40 Kč,1 empty columns,",
                "0009: 7,05.05.2015,17:53,Bruto ,789,50 Kč,1 empty columns,",
                "0010: 8,19.05.2015,17:51,Bruto ,1 477,90 Kč,1 empty columns,",
                "0011: 9,03.07.2015,16:10,OMV,1 382,50 Kč,1 empty columns,",
                "0012: 10,12.07.2015,15:07,Bruto ,850,90 Kč,1 empty columns,",
                "0013: 11,26.07.2015,16:56,Bruto ,1 500,20 Kč,Tvoje,",
                "0014: 12,5 empty columns,",
                "0015: 13,5 empty columns,",
                "0016: 14,5 empty columns,",
                "0017: 15,5 empty columns,",
                "0018: 16,5 empty columns,",
                "0019: 17,5 empty columns,",
                "0020: 18,5 empty columns,",
                "0021: 19,5 empty columns,",
                "0022: 20,5 empty columns,",
                "0023: 21,5 empty columns,",
                "0024: 22,5 empty columns,",
                "0025: 23,5 empty columns,",
                "0026: 24,5 empty columns,",
                "0027: 25,5 empty columns,",
                "0028: 26,5 empty columns,",
                "0029: 27,5 empty columns,",
                "0030: 28,5 empty columns,",
                "0031: 29,5 empty columns,",
                "0032: 30,5 empty columns,",
                "1048543 empty rows"
        };


        Assert.assertArrayEquals(expected, outputLines);
    }

    @Test
    public void testReadValues() throws Exception {
        File file = new File("src/test/resources/test1sheets.ods");
        extractor.read(ExcelExtractSpec.valueOf(file, "List1!A2,D2;List2!A1"));
        String out = outputStream.toString();
        String[] outputLines = out.split(System.lineSeparator());

        String[] expected = {
                "test1sheets.ods:LEDEN,DUBEN,List2,"
        };

        Assert.assertArrayEquals(expected, outputLines);
    }

    @Test(expected = NoSuchElementException.class)
    public void testReadValues_NotExists() throws Exception {
        File file = new File("src/test/resources/test2exp.ods");
        extractor.read(ExcelExtractSpec.valueOf(file, "List1!B2"));
    }
}